# Proov

## Usage

Include the `[proov]` shortcode in your post content.

### Flavors

You can include different types of contents by specifying the content attribute. 

For example: `[proov content="short"]`

#### Available content flavors

- base

## Custom content

While the plugin's sample contents can't cover all the imaginable content formats you can load your own.

Create the `proov/content/complex-table.php` file in your theme directory.

Include it with `[proov content="complex-table"]`

The file name **should match** the content name.

## Filters

In case you want to create your own sample content in a different place then the `proov/content/` folder, you can use the `proov_content_path` filter to overwrite it.

```
add_filter( 'proov_content_path', function() {

    return trailingslashit( get_stylesheet_directory() ) . "custom-content/";

} );
```

## Contribute

…

### NB

After creating this plugin I found the [Dummy Text Shortcode](http://wordpress.org/extend/plugins/dummy-text-shortcode/) plugin developed by [Norcross](http://andrewnorcross.com/about/).