<h1>Quod cum dixissent, ille contra.</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ea possunt paria non esse. <a href='#'>Vide, quantum, inquam, fallare, Torquate.</a> Quod ea non occurrentia fingunt, vincunt Aristonem; Nihilo beatiorem esse Metellum quam Regulum. Duo Reges: constructio interrete. Idem iste, inquam, de voluptate quid sentit? Iam id ipsum absurdum, maximum malum neglegi. </p>

<p>At iste non dolendi status non vocatur voluptas. Tibi hoc incredibile, quod beatissimum. Quae in controversiam veniunt, de iis, si placet, disseramus. Haec quo modo conveniant, non sane intellego. <em>Hoc mihi cum tuo fratre convenit.</em> <a href='#'>Sed tamen intellego quid velit.</a> Traditur, inquit, ab Epicuro ratio neglegendi doloris. Nam de isto magna dissensio est. </p>

<ul>
    <li>Num quid tale Democritus?</li>
    <li>Sed non alienum est, quo facilius vis verbi intellegatur, rationem huius verbi faciendi Zenonis exponere.</li>
    <li>Quamquam haec quidem praeposita recte et reiecta dicere licebit.</li>
    <li>Cupit enim dícere nihil posse ad beatam vitam deesse sapienti.</li>
</ul>

<ol>
    <li>Facit igitur Lucius noster prudenter, qui audire de summo bono potissimum velit;</li>
    <li>Quantum Aristoxeni ingenium consumptum videmus in musicis?</li>
    <li>Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?</li>
    <li>Et nunc quidem quod eam tuetur, ut de vite potissimum loquar, est id extrinsecus;</li>
</ol>

<dl>
    <dt>Iam enim adesse poterit.</dt>
    <dd>Sin autem est in ea, quod quidam volunt, nihil impedit hanc nostram comprehensionem summi boni.</dd>
    <dt>Venit ad extremum;</dt>
    <dd>Illa videamus, quae a te de amicitia dicta sunt.</dd>
</dl>

<ol>
    <li>Sic consequentibus vestris sublatis prima tolluntur.</li>
    <li>Cur haec eadem Democritus?</li>
</ol>

<ul>
    <li>Oculorum, inquit Plato, est in nobis sensus acerrimus, quibus sapientiam non cernimus.</li>
    <li>Qua tu etiam inprudens utebare non numquam.</li>
</ul>

<h5>Ne amores quidem sanctos a sapiente alienos esse arbitrantur.</h5>

<p>Quasi ego id curem, quid ille aiat aut neget. Quamquam id quidem licebit iis existimare, qui legerint. Dolere malum est: in crucem qui agitur, beatus esse non potest. Et harum quidem rerum facilis est et expedita distinctio. Prioris generis est docilitas, memoria; Certe, nisi voluptatem tanti aestimaretis. <a href='#'>Quid iudicant sensus?</a> Aeque enim contingit omnibus fidibus, ut incontentae sint. Inde sermone vario <em>[redacted]</em> illa a Dipylo stadia confecimus. </p>

<pre>
Itaque nostrum est-quod nostrum dico, artis est-ad ea
principia, quae accepimus.

Parvi enim primo ortu sic iacent, tamquam omnino sine animo
sint.
</pre>

<h3>Qui bonum omne in virtute ponit, is potest dicere perfici beatam vitam perfectione virtutis;</h3>

<p>Expressa vero in iis aetatibus, quae iam confirmatae sunt. Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. Mihi enim erit isdem istis fortasse iam utendum. <strong>Itaque contra est, ac dicitis;</strong> Quid, quod res alia tota est? Multoque hoc melius nos veriusque quam Stoici. Scaevolam M. Igitur neque stultorum quisquam beatus neque sapientium non beatus. <a href='#'>Hoc mihi cum tuo fratre convenit.</a> </p>

<p><code>Aperiendum est igitur, quid sit voluptas;</code> Solum praeterea formosum, solum liberum, solum civem, stultost; Sed ut iis bonis erigimur, quae expectamus, sic laetamur iis, quae recordamur. <em>Quid vero?</em> Sed nimis multa. Ergo hoc quidem apparet, nos ad agendum esse natos. Transfer idem ad modestiam vel temperantiam, quae est moderatio cupiditatum rationi oboediens. </p>

<p>Quid est, quod ab ea absolvi et perfici debeat? <mark>Refert tamen, quo modo.</mark> <code>Refert tamen, quo modo.</code> Sic vester sapiens magno aliquo emolumento commotus cicuta, si opus erit, dimicabit. Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus; Cyrenaici quidem non recusant; <a href='#'>Certe, nisi voluptatem tanti aestimaretis.</a> Illum mallem levares, quo optimum atque humanissimum virum, Cn. </p>

<h4>Sed tamen intellego quid velit.</h4>

<p><em>Quae cum dixisset, finem ille.</em> Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere. <mark>Egone quaeris, inquit, quid sentiam?</mark> Memini vero, inquam; Duo enim genera quae erant, fecit tria. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. <strong>Summum a vobis bonum voluptas dicitur.</strong> Tibi hoc incredibile, quod beatissimum. Haec para/doca illi, nos admirabilia dicamus. </p>

<dl>
    <dt>Quis enim redargueret?</dt>
    <dd>Quarum cum una sit, qua mores conformari putantur, differo eam partem, quae quasi stirps ets huius quaestionis.</dd>
    <dt>Quid Zeno?</dt>
    <dd>Tu enim ista lenius, hic Stoicorum more nos vexat.</dd>
    <dt>Magna laus.</dt>
    <dd>Venit enim mihi Platonis in mentem, quem accepimus primum hic disputare solitum;</dd>
    <dt>Ita prorsus, inquam;</dt>
    <dd>Deinde disputat, quod cuiusque generis animantium statui deceat extremum.</dd>
</dl>

<blockquote cite='http://loripsum.net'>
    Ita finis bonorum existit secundum naturam vivere sic affectum, ut optime is affici possit ad naturamque accommodatissime.
</blockquote>

<blockquote cite='http://loripsum.net'>
    Qua in vita tantum abest ut voluptates consectentur, etiam curas, sollicitudines, vigilias perferunt optimaque parte hominis, quae in nobis divina ducenda est, ingenii et mentis acie fruuntur nec voluptatem requirentes nec fugientes laborem.
</blockquote>

<pre>
Quae quidem omnia et innumerabilia praeterea quis est quin
intellegat et eos qui fecerint dignitatis splendore ductos
inmemores fuisse utilitatum suarum nosque, cum ea laudemus,
nulla alla re nisi honestate duci?

Iam id ipsum absurdum, maximum malum neglegi.
</pre>

<p>Nemo igitur esse beatus potest. At, si voluptas esset bonum, desideraret. Quid turpius quam sapientis vitam ex insipientium sermone pendere? <a href='#'>Cur iustitia laudatur?</a> Itaque contra est, ac dicitis; Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. </p>

<h2>Ostendit pedes et pectus.</h2>

<p>Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; Videsne quam sit magna dissensio? Quamvis enim depravatae non sint, pravae tamen esse possunt. Hoc loco tenere se Triarius non potuit. Quo modo autem philosophus loquitur? </p>

<p>Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Sed ego in hoc resisto; Quid igitur, inquit, eos responsuros putas? <a href='#'>Tu quidem reddes;</a> <mark>Etiam beatissimum?</mark> Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; </p>