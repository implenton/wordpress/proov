<?php
/*
 * Plugin Name:       Proov
 * Description:       Include sample HTML markups with shortcode
 * Plugin URI:        https://gitlab.com/implenton/wordpress/proov
 * Version:           0.3.2
 * Author:            implenton
 * Author URI:        https://implenton.com/
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * GitLab Plugin URI: implenton/wordpress/proov
 */

namespace Proov;

add_shortcode( 'proov', __NAMESPACE__ . '\shortcode' );

function shortcode( $atts ) {

    $atts = shortcode_atts( [
        'content' => 'base',
    ], $atts, 'proov' );

    return get_content( $atts['content'] );

}

function get_content( $filename ) {

    $theme_path = trailingslashit( get_stylesheet_directory() ) . "proov/content/";

    $directories = [
        apply_filters( 'proov_content_path', $theme_path ),
        plugin_dir_path( __FILE__ ) . "content/",
    ];

    foreach ( $directories as $directory ) {

        $file = trailingslashit( $directory ) . $filename . ".php";

        if ( file_exists( $file ) ) {

            ob_start();

            include $file;

            return ob_get_clean();

        }

    }

    return __( 'Content file does not exists', 'proov' );

}